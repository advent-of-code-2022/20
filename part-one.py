#!/usr/bin/env python3

import os

class Item:
    items = []
    zero = None

    def for_all(func):
        [func(item) for item in Item.items]

    def get_zero_offset(offset):
        item = Item.zero
        while offset != 0:
            if offset > 0:
                item = item.after
                offset -= 1
            else:
                item = item.before
                offset += 1
        return item

    def __init__(self, value):
        self.value = value
        if self.value == 0:
            Item.zero = self
        self.before = None
        self.after = None
        self._index = len(Item.items)
        Item.items.append(self)
    
    def __repr__(self):
        return f"{self.before.value} -> {self.value} -> {self.after.value}"

    def move_left(self, count):
        if count <= 0:
            return
        new_after = self
        while count > 0:
            count -= 1
            new_after = new_after.before
        new_before = new_after.before
        old_before = self.before
        old_after = self.after
        new_before.after = self
        self.before = new_before
        self.after = new_after
        new_after.before = self
        old_before.after = old_after
        old_after.before = old_before

    def move_right(self, count):
        if count <= 0:
            return
        new_before = self
        while count > 0:
            count -= 1
            new_before = new_before.after
        new_after = new_before.after
        old_before = self.before
        old_after = self.after
        old_before.after = old_after
        old_after.before = old_before
        new_before.after = self
        self.before = new_before
        self.after = new_after
        new_after.before = self

    def get_offset(self, offset):
        if offset < 0:
            return self.before.get_offset(offset + 1)
        elif offset > 0:
            return self.after.get_offset(offset - 1)
        else:
            return self

    def hook_up_neighbours(self):
        if self._index == 0:
            self.before = Item.items[len(Item.items)-1]
        else:
            self.before = Item.items[self._index - 1]
        if self._index == len(Item.items) - 1:
            self.after = Item.items[0]
        else:
            self.after = Item.items[self._index + 1]

def answer(input_file):
    with open(input_file, "r") as input_data:
        data = input_data.read().split("\n")

    for item in data:
        Item(int(item))
    
    Item.for_all(lambda item: item.hook_up_neighbours())

    for item in Item.items:
        if item.value > 0:
            item.move_right(item.value % (len(Item.items)-1))
        elif item.value < 0:
            item.move_left((0-item.value) % (len(Item.items)-1))

    values = [Item.get_zero_offset(1000).value, Item.get_zero_offset(2000).value, Item.get_zero_offset(3000).value]
    answer = sum(values)

    print(f"The answer is *** {answer} ***")

input_file = os.path.join(os.path.dirname(__file__), "input")
answer(input_file)
